let
  pkgs = import <nixpkgs> {};
  unPkgs = import <nixpkgs> {
    overlays = [
      (old: n: {
        mkl = n.mkl.overrideAttrs (a: {
          meta = {};
        });
      })
    ];
  };
  deps = p: with p; [
    pytorch torchvision jupyter
  ];
in
pkgs.mkShell {
  buildInputs = with unPkgs; [
    (python3.withPackages deps)
  ];
}
